package or.spring.boot.hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class AlcoholTestApplication implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(AlcoholTestApplication.class, args);
	}
	/*@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/static/**")
        .addResourceLocations("/resources/");

	}*/

}

